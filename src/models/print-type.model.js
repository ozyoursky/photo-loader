/**
 * Print type model
 * @class PrintType
 * @property {String} label
 * @property {PrintFormat[]} sizes
 * @property {String} thumbnail
 */
export class PrintType {
  /**
   * @constructor
   * @param {String} label print type label
   * @param {String} thumbnail thumbnail url
   * @param {PrintFormat[]} formats print type size
   */
  constructor(label, thumbnail, formats) {
    this.label = label;
    this.formats = formats;
    this.thumbnail = thumbnail;
  }
}
