/**
 * Photo size model
 * @class PrintFormat
 * @property {String} size
 * @property {number} prise
 */
export class PrintFormat {
  /**
   * @constructor
   * @param {String} label label value
   * @param {number} price price for this format
   */
  constructor(label, price) {
    this.label = label;
    this.price = price;
  }
}
