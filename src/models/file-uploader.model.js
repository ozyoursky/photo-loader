import { FileUploadApi } from '../api/file.upload.api';
import { RandomUtils } from '../utils/random.utils';

/**
 * @class FileUploader
 *
 * @property {Boolean} uploaded
 * @property {File} file
 * @property {String} storeId
 * @property {Boolean} inProgress
 * @property {String} id
 */
export class FileUploader {
  /**
   * @constructor
   * @param {String} storeId
   * @param {File} file
   */
  constructor(storeId, file) {
    this.storeId = storeId;
    this.file = file;
    this.inProgress = false;
    this.uploaded = false;
    this.progress = 0;
    this.id = RandomUtils.createRandomString();
  }

  /**
   * Start upload
   * @returns {Promise<void>}
   */
  async upload() {
    const onProgress = progressEvent => this._onUploadProgress(progressEvent);
    const onCancelFunctionReady = cancelFunction => this._onCancelFunctionReady(cancelFunction);
    this.inProgress = true;

    await FileUploadApi.uploadFile(
      this.storeId,
      this.file,
      onProgress,
      onCancelFunctionReady,
    );

    this.inProgress = false;
    this.uploaded = true;
  }

  /**
   * Cancel upload if it possible
   * @returns {Promise<void>}
   */
  async cancelUpload() {
    if (this.inProgress && this.cancel) {
      this.cancel();
    }
  }

  /**
   * Handle upload progress event
   * @param progressEvent
   * @private
   */
  _onUploadProgress(progressEvent) {
    this.progress = (progressEvent.loaded / progressEvent.total) * 100;
  }

  /**
   * Handle cancel function ready event
   * @param {Function} cancelFunction
   * @private
   */
  _onCancelFunctionReady(cancelFunction) {
    this.cancel = cancelFunction;
  }
}
