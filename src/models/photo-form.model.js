/**
 * Photo form data model
 * @class PrintFormData
 *
 * @property {String} name
 * @property {String} phone
 * @property {String} address
 * @property {String} wishes
 * @property {PrintType} printType
 * @property {PrintFormat} printFormat
 * @property {String} promoCode
 * @property {String} region
 * @property {String} deliveryType
 * @property {Function} i18n
 */
export class PrintFormData {
  /**
   * @constructor
   * @param {String} name
   * @param {String} phone
   * @param {String} address
   * @param {String} wishes
   * @param {PrintType} printType
   * @param {PrintFormat} printFormat
   * @param {String} promoCode
   * @param {String} region
   * @param {String} deliveryType
   * @param {Function<String, *>} i18n
   */
  constructor(
    {
      name,
      phone,
      address,
      wishes,
      printType,
      printFormat,
      promoCode,
      region,
      i18n,
    },
  ) {
    this.name = name;
    this.phone = phone;
    this.address = address;
    this.printType = printType;
    this.printFormat = printFormat;
    this.region = region;
    this.wishes = wishes;
    this.promoCode = promoCode;
    this.i18n = i18n;
  }

  toLocaledDto() {
    return {
      [this.i18n('labels.name')]: this.name,
      [this.i18n('labels.phone')]: this.phone,
      [this.i18n('labels.address')]: this.address,
      [this.i18n('labels.printType')]: this.printType.label,
      [this.i18n('labels.printFormat')]: this.printFormat.label,
      [this.i18n('labels.promoCode')]: this.promoCode || this.i18n('labels.no'),
      [this.i18n('labels.wishes')]: this.wishes || this.i18n('labels.no'),
    };
  }
}
