/**
 * @class ImageSelectElement
 *
 * @property {String} imageUrl
 * @property {String} label
 * @property {Object} value
 */
export class ImageSelectElement {
  /**
   * @constructor
   * @param {String} imageUrl image url
   * @param {String} label select element label
   * @param {Object} value selected value
   */
  constructor(imageUrl, label, value) {
    this.imageUrl = imageUrl;
    this.label = label;
    this.value = value;
  }

}
