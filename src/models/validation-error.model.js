/**
 * Validation error implementation
 *
 * @class ValidationError
 *
 * @property {String} header
 * @property {Array<String>} messages
 */
export class ValidationError {
  /**
   * @constructor
   * @param {String} header error header
   * @param {Array<String>} messages error messages
   */
  constructor(header, messages) {
    this.header = header;
    this.messages = messages;
  }
}
