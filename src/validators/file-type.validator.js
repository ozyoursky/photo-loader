import BaseValidator from './base.validator';
import { FileTypeUtils } from '../utils/file-type.utils';
import { ValidationError } from '../models/validation-error.model';

/**
 * File type validation implementation
 * @class FileTypeValidator
 *
 * @property {Array<String>} allowedFileTypes
 * @property {Array<String>} allowedExtensions
 * @property {Function} i18n
 */
export default class FileTypeValidator extends BaseValidator {
  /**
   * @constructor
   * @param {Array<String>} allowedFileTypes allowed file types
   * @param {Array<String>} allowedExtensions allowed file extensions
   * @param {Function} i18n translator
   */
  constructor(allowedFileTypes, allowedExtensions, i18n) {
    super();
    this.allowedFileTypes = allowedFileTypes;
    this.allowedOverrideExtensions = allowedExtensions;
    this.i18n = i18n;
  }

  /**
   * @inheritDoc
   * @param {File} file file for validation
   * @returns {ValidationError|null}
   */
  validate(file) {
    const endsWithAllowedExtensions = this.allowedOverrideExtensions.some(ext => file.name.endsWith(ext));
    const hasCorrectFileType = FileTypeUtils.isFileTypeValid(file, this.allowedFileTypes);

    if (hasCorrectFileType || endsWithAllowedExtensions) {
      return null;
    }

    return new ValidationError(this.i18n('errors.incorrectFileType'), [
      `${this.i18n('labels.allowedTypes')}: ${this.allowedTypesString}`,
      `${this.i18n('labels.conflictFile')} ${file.name}`,
    ]);
  }

  get allowedTypesString() {
    return this.allowedFileTypes.map(fileType => FileTypeUtils.getFileTypename(fileType)).join(', ');
  }
}
