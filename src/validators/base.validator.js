export default class BaseValidator {
  /**
   * Validate data
   * @abstract
   * @param {Object | String} data validating data
   * @returns {ValidationError}
   */
  validate(data) {
  }
}
