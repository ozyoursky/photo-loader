import Vue from 'vue';
import VueI18n from 'vue-i18n';
import ru from '../translations/ru';
import { LocaleConstants } from '../constants/locale.constants';

Vue.use(VueI18n);

const messages = {
  [LocaleConstants.languages.BY]: ru,
  [LocaleConstants.languages.RU]: ru,
};

export default new VueI18n({
  locale: LocaleConstants.languages.RU,
  messages,
});
