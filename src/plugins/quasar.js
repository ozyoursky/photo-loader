import Vue from 'vue';

import '../styles/quasar.styl';

import 'quasar/dist/quasar.ie.polyfills';
import lang from 'quasar/lang/ru.js';
import '@quasar/extras/material-icons/material-icons.css';
import Quasar, {
  QVirtualScroll,
} from 'quasar';

Vue.use(Quasar, {
  config: {},
  components: {
    QVirtualScroll,
  },
  lang: lang,
});
