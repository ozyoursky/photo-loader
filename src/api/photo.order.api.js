import axios from 'axios';
import { ApiConstants } from '../constants/api.constants';

export class PhotoOrderApi {
  static async createOrder() {
    const result = await axios.get(`${ApiConstants.API_ENDPOINT}/${ApiConstants.CREATE_ORDER}`);
    return result && result.data;
  }

  /**
   * Submit order
   * @param {String} storeId id of store
   * @param {{formData: PrintFormData, removedFiles: Array<String>}} photoFormData form data
   * @returns {Promise<AxiosResponse<T>>}
   */
  static submitOrder(storeId, photoFormData) {
    return axios.post(
      `${ApiConstants.API_ENDPOINT}/${storeId}/${ApiConstants.SUBMIT}?region=${photoFormData.formData.region}`,
      {
        formData: photoFormData.formData.toLocaledDto(),
        removedFiles: photoFormData.removedFiles,
      }
    );
  }

  static async getSettings() {
    const result = await axios.get(`${ApiConstants.API_ENDPOINT}/${ApiConstants.SETTINGS}`);
    return result && result.data;
  }
}
