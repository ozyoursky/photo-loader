import { ApiConstants } from '../constants/api.constants';
import axios from 'axios';

const CancelToken = axios.CancelToken;

export class FileUploadApi {
  static uploadFile(storeId, file, onProgress, onCancelFunctionReady) {
    const formData = new FormData();
    formData.append(ApiConstants.FILE, file);

    return axios.post(
      `${ApiConstants.API_ENDPOINT}/${storeId}/${ApiConstants.FILE}`,
      formData,
      {
        onUploadProgress: onProgress,
        cancelToken: new CancelToken(onCancelFunctionReady),
      },
    );
  }
}
