export class FileTransformUtils {
  static async toDataUrl(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    return new Promise(((resolve, reject) => {
      reader.onload = loadEvent => resolve(loadEvent.target.result);
      reader.onerror = reject;
    }));
  }
}
