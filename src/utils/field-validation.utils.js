export class FieldValidationUtils {
  static getMinLengthValidator(minLength) {
    return str => (str || '').length >= minLength || `Минимальная длина: ${minLength}`;
  }

  static getMaxLengthValidator(maxLength) {
    return str => (str || '').length <= maxLength || `Максимальная длина: ${maxLength}`;
  }

  static getPhoneValidator() {
    return value => !!(value || '').match(/\([0-9]{2}\) [0-9]{3} [0-9]{2} [0-9]{2}$/)
      || 'Пример: (12) 345 67 89';
  }

  static getRequiredValidator() {
    return value => !!value || 'Это поле обязательно для заполнения';
  }
}
