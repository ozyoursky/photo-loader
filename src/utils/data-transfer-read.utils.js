/**
 * @typedef FileSystemFileEntry
 * @typedef FileSystemReader
 * @typedef DirectoryReader
 */

export default class DataTransferReadUtils {
  /**
   * Convert file entry to file
   * @param {FileSystemFileEntry} fileEntry
   * @returns {Promise<unknown>}
   */
  static async fileEntryToFile(fileEntry) {
    return new Promise((resolve, reject) => fileEntry.file(resolve, reject));
  }

  /**
   * Transform all directories and entries to flat files array
   * @param dataTransferItemList
   * @returns {Promise<[]>}
   */
  static async getAllFileEntries(dataTransferItemList) {
    let fileEntries = [];
    const queue = DataTransferReadUtils._getAllDataTransferItems(dataTransferItemList);

    while (queue.length > 0) {
      const entry = queue.shift();

      if (entry.isFile) {
        fileEntries.push(entry);
      }

      if (entry.isDirectory) {
        const reader = entry.createReader();
        const directoryEntries = await DataTransferReadUtils._readAllDirectoryEntries(reader);
        queue.push(...directoryEntries);
      }
    }
    return fileEntries;
  }

  /**
   * Convert dataTransferItemList to file entries
   * @param {DataTransferItemList} dataTransferItemList
   * @returns {Array<FileSystemFileEntry>}
   * @private
   */
  static _getAllDataTransferItems(dataTransferItemList) {
    let queue = [];

    Array.from(dataTransferItemList).forEach(
      item => item && queue.push(item.webkitGetAsEntry()),
    );

    return queue;
  }

  /**
   * Read all directory entries
   * @param directoryReader
   * @returns {Promise<[]>}
   * @private
   */
  static async _readAllDirectoryEntries(directoryReader) {
    const entries = [];
    let readEntries = await DataTransferReadUtils._getReadEntriesPromise(directoryReader);
    while (readEntries.length > 0) {
      entries.push(...readEntries);
      readEntries = await DataTransferReadUtils._getReadEntriesPromise(directoryReader);
    }
    return entries;
  }

  /**
   *
   * @param {DirectoryReader} directoryReader
   * @returns {Promise<FileSystemReader>}
   * @private
   */
  static async _getReadEntriesPromise(directoryReader) {
    return await new Promise((resolve, reject) => {
      directoryReader.readEntries(resolve, reject);
    });
  }
}
