export class PendingUtils {
  static async pending(milliseconds) {
    return new Promise(resolve => setTimeout(() => resolve(), milliseconds));
  }
}
