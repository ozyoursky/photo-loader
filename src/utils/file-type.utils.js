import { FileTypesConstants } from '../constants/file-types.constants';

export class FileTypeUtils {
  static isFileTypeValid(file, allowedTypes) {
    return allowedTypes.includes(file.type);
  }

  static getFileTypename(fileType) {
    const separatorIndex = fileType.indexOf('/');
    return separatorIndex === -1
      ? fileType
      : fileType.slice(separatorIndex + 1, fileType.length);
  }

  static isImage(file) {
    return file.type === FileTypesConstants.image.JPEG
      || file.type === FileTypesConstants.image.PNG;
  }

  static isArchive(file) {
    return FileTypesConstants.ARCHIVE_TYPES.includes(file.type);
  }
}
