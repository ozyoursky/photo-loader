export const ImageConstants = {
  LOGO: 'https://photoloader-bucket.s3.eu-central-1.amazonaws.com/logo.jpg',
  ARCHIVE_IMAGE: 'https://photoloader-bucket.s3.eu-central-1.amazonaws.com/archive.png',
  PHOTO_PLACEHOLDER: 'https://photoloader-bucket.s3.eu-central-1.amazonaws.com/large-photo.png',
};
