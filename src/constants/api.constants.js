export const ApiConstants = {
  API_ENDPOINT: process.env.PHOTOLOADER_HOST || 'https://www.vebry-dev.com',
  CREATE_ORDER: 'init',
  FILE: 'file',
  SUBMIT: 'submit',
  SETTINGS: 'settings',
};
