export const FileTypesConstants = {
  image: {
    JPEG: 'image/jpeg',
    PNG: 'image/png',
  },

  ARCHIVE_TYPES: ['application/x-zip-compressed', 'application/zip'],
  IMAGE_TYPES: ['image/jpeg', 'image/png'],
};
