import Vue from 'vue';
import App from './root.vue';
import vuetify from './plugins/vuetify';
import './styles/common.scss';
import './plugins/vue-the-mask';
import i18n from './plugins/i18n';
import './plugins/quasar';

Vue.config.productionTip = false;

new Vue({
  vuetify,
  i18n,
  render: h => h(App),
}).$mount('#app');
