module.exports = {
  publicPath: '',

  devServer: {
    port: 3000,
    disableHostCheck: true,
    overlay: {
      errors: true,
    },
  },

  pluginOptions: {
    quasar: {
      treeShake: true,
    },
  },

  transpileDependencies: [
    /[\\\/]node_modules[\\\/]quasar[\\\/]/,
  ],
};
